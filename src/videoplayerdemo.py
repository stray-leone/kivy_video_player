import kivy
kivy.require('1.9.0')

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.widget import Widget
from kivy.uix.videoplayer import VideoPlayer
from kivy.uix.videoplayer import VideoPlayerAnnotation
from kivy.uix.image import Image
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.animation import Animation
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.screenmanager import FadeTransition, ShaderTransition, SlideTransition
import os
import sys

parent= Widget()
button= Button()

manager = None
screens = []

video_dir = "../video_files/"
video_list = os.listdir(video_dir)
video_list = [ video_dir+v for v in video_list]
for v in video_list:
    print v



#video_list = [video_fpath, video_fpath1]

class PunchVideoPlyaer(VideoPlayer):

    def on_state(self, instance, value):
        VideoPlayer.on_state(self, instance, value)
        global screens
        global manager
        if value == "stop":
            print "stop source : "+self.source
            screens.pop(0)
            target_screen = screens[0]

            try:
                manager.switch_to(target_screen)
            except:
                exit()

            if video_list:
                next_video = video_list.pop(0)
                message = "next excercise : "+ next_video
                screens.append(VideoScreen(source= next_video))
            else:
                message = "Final exercise"

            # clear annotaion list
            target_screen.video._annotations_labels= []

            # add annotation
            ann = VideoPlayerAnnotation(start= 10, duration= 10)
            setattr(ann,"text",message)
            target_screen.video._annotations_labels.append(ann)

            # change preview image
            target_screen.video.root.ids.image.source = "../IMG_0788.png"

            # start play video
            target_screen.video.state = "play"


class VideoScreen(Screen):
    def __init__(self, **kwargs):
        super(VideoScreen, self).__init__(**kwargs)
        #self.add_widget(Button(text = 'test'))
        v =  kwargs["source"]

        root = FloatLayout()

        # video
        #video= VideoPlayer(fullscreen=True, allow_stretch=True, source=video_fpath, state='play', options={'eos': 'stop'})
        self.video= PunchVideoPlyaer(fullscreen=True, allow_stretch=True, source=v, state='pause', options={'eos': 'stop'}) #indicates whether to play, pause, or stop
        self.video.root = root

        # layout which have image
        image = Image(source="../index.jpeg",size_hint=(None, None),size=(150, 150))
        layout = AnchorLayout(anchor_x='right', anchor_y='top', padding=[0, 20, 80, 20]) #left, top, right, bottom
        layout.add_widget(image)

        root.ids["image"] = image
        root.ids["layout"] = layout

        root.add_widget(self.video)
        root.add_widget(layout)

        self.add_widget(root)


class MyApp(App):
    def build(self):
        global screens
        global manager

        # And create your transition
        tr = SlideTransition(duration=3)

        manager = ScreenManager(transition=tr)

        screens.append(VideoScreen(source = video_list.pop(0)))
        screens.append(VideoScreen(source = video_list.pop(0)))

        manager.switch_to(screens[0])
        screens[0].video.state = "play"

        return manager


if __name__ == '__main__':
    app = MyApp()
    #r = app.build()
    app.run()

